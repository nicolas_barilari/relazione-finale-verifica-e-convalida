# Relazione Finale Verifica e Convalida del Software - Nicolas Barilari

Sviluppo della relazione finale per il corso di "Verifica e Convalida del Software", Laurea Magistrale in Informatica, Università degli Studi di Milano.

| Testing con UI Automator su Android |
|-------------------------------------|
| [Presentazione](https://gitlab.com/nicolas_barilari/relazione-finale-verifica-e-convalida/-/tree/presentazione) |
| [Demo](https://gitlab.com/nicolas_barilari/relazione-finale-verifica-e-convalida/-/tree/demo) |
